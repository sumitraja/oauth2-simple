{-# OPTIONS_GHC -fno-warn-orphans #-}
-- | A simple OAuth2 implementation with nonce state to prevent forgery attacks.
--
-- Originally based on [frekletonj](https://gist.github.com/freckletonj/17eec8959718cb251f29af3645112f4a)'s
-- oauth gist, this library should be simple and fast enough for moderate use.
module Network.OAuth2
    ( -- * Construct an oauth state
      newOAuthState, newOAuthStateWith
      -- * Default configurations
    , oauthStateless, oauthStateNonce, verifyOAuthNonce
      -- * Typical client endpoints
    , getAuthorize, getAuthorized, refreshAuthorized
      -- * Types
    , OAuthStateConfig(..)
    , OAuth2(..), OAuthState
    , TokenByteStringAcquire
    , Endpoint
    , Code
    , Token(..)
    , Nonce
    , GrantType (..)
    , TokenType (..)
    ) where

import           Control.Concurrent (threadDelay,forkIO)
import           Control.Monad (forever)
import           Data.IORef
import           Data.Text (Text)
import           Data.Text.Encoding (decodeUtf8)
import qualified Data.Text as T
import           Data.List (intercalate)
import qualified Data.Set as Set
import           Data.Monoid
import           Text.Read (readMaybe)
import           Control.Monad.IO.Class
import           Data.Aeson
import           Data.Aeson.Types
import           System.RandomString
import           Data.ByteString.Lazy as LB hiding (map, intercalate)
import           GHC.Generics
import           Web.HttpApiData
import           Web.FormUrlEncoded
import           Text.Casing (quietSnake)
import           Protolude

data OAuthState
    = OAuthState (IORef ((Integer, Set.Set (Integer,Text))))
    | OAuthStateless

type Sep = Text

data OAuth2 = OAuth2 { oauthClientId :: Text
                     , oauthClientSecret :: Text
                     , oauthOAuthorizeEndpoint :: Text
                     , oauthAccessTokenEndpoint :: Text
                     , oauthCallback :: Text
                     , oauthScopes :: (Sep, [Scope])
                     } deriving (Show, Eq)

type Endpoint = Text
type Code = Text

type TokenByteStringAcquire = Endpoint -> TokenRequest -> IO (Either Text LByteString)

type Scope = Text

data GrantType = RefreshToken | AuthorizationCode deriving (Generic, Show, Eq)

instance FromJSON GrantType where
  parseJSON str@(String gt) 
    | T.toLower gt == "authorization_code" = return AuthorizationCode
    | T.toLower gt == "refresh_token" = return RefreshToken
    | otherwise = typeMismatch "GrantType " str
  parseJSON s = typeMismatch "GrantType " s

instance FromHttpApiData GrantType where
  parseQueryParam gt
    | T.toLower gt == "authorization_code" = return AuthorizationCode
    | T.toLower gt == "refresh_token" = return RefreshToken
    | otherwise = Left (T.pack $ "Unknown GrantType " <> (show gt))

instance ToHttpApiData GrantType where
  toQueryParam AuthorizationCode = "authorization_code"
  toQueryParam RefreshToken = "refresh_token"
  
data TokenType = Bearer deriving (Eq, Show, Generic)

instance FromJSON TokenType where
  parseJSON str@(String gt)
    | T.toLower gt == "bearer" = return Bearer
    | otherwise = typeMismatch "TokenType " str
  parseJSON s = typeMismatch "TokenType " s

instance ToJSON TokenType where
   toJSON Bearer = String "bearer"
  
data Token = Token {
  token_type :: TokenType,
  access_token :: Maybe Text,
  refresh_token :: Maybe Text,
  expires_in :: Integer
} deriving (Generic, FromJSON)

instance ToJSON Token where
    toEncoding = genericToEncoding defaultOptions

data TokenRequest = TokenRequest {
  clientId :: Text,
  clientSecret :: Text,
  grantType :: GrantType,
  code :: Maybe Text,
  refreshToken :: Maybe Text,
  redirectUri :: Maybe Text
} deriving (Show, Eq, Generic)

instance ToForm TokenRequest where
  toForm = genericToForm (FormOptions {Web.FormUrlEncoded.fieldLabelModifier = quietSnake })

renderScopes :: (Sep, [Scope]) -> Text
renderScopes (sp, scopes) = T.pack $ intercalate (T.unpack sp) $ fmap T.unpack scopes

----------

authEndpoint :: OAuth2 -> Text -> Text
authEndpoint oa theState =
  mconcat $ [ oauthOAuthorizeEndpoint oa
            , "?client_id=", oauthClientId oa
            , "&response_type=", "code"
            , "&redirect_uri=", oauthCallback oa
            , "&scope=", renderScopes (oauthScopes oa) ]
             <> if T.length theState > 0
                then [ "&state=", theState ]
                else mempty

tokenEndpoint :: GrantType -> Text -> OAuth2 -> TokenRequest
tokenEndpoint RefreshToken c oa = TokenRequest (oauthClientId oa) (oauthClientSecret oa) RefreshToken Nothing (Just c)  Nothing
tokenEndpoint AuthorizationCode c oa = TokenRequest (oauthClientId oa) (oauthClientSecret oa) AuthorizationCode (Just c) Nothing (Just $ oauthCallback oa)

----------

data OAuthStateConfig
      = OAuthStatelessConfig
      | OAuthStateConfig { nonceLifetime :: Integer
                         -- Time from URL generation that login can occur
                         -- (seconds).  This lifetime is only enforced
                         -- approximately.
                         }

oauthStateNonce :: OAuthStateConfig
oauthStateNonce = OAuthStateConfig { nonceLifetime = (10*60) }

oauthStateless :: OAuthStateConfig
oauthStateless = OAuthStatelessConfig

-- Step 0. Get an auth state so redirects won't work

-- |Obtain a new OAuth state.
--
-- In oauth terms: Each resource owner needs a state, a set of valid nonces,
-- to validate oauth requests.  When an application authenticates to the oauth
-- provider this state is included to eliminate forgery attacks.
newOAuthState :: MonadIO m => m OAuthState
newOAuthState = newOAuthStateWith oauthStateless

newOAuthStateWith :: MonadIO m => OAuthStateConfig -> m OAuthState
newOAuthStateWith OAuthStatelessConfig = pure OAuthStateless
newOAuthStateWith cfg = liftIO $ do
    mv <- newIORef (0,mempty)
    let s = OAuthState mv
    _ <- forkIO $ collectGarbage s
    pure s
 where
  collectGarbage OAuthStateless = pure ()
  collectGarbage (OAuthState s) = forever $ do
       threadDelay (1000*1000*30) -- 30 seconds
       atomicModifyIORef s $ \(cnt,set) ->
           let tooOldCount = cnt - (nonceLifetime cfg `div` 30)
               !newCnt = cnt+1
               !(_,!newSet) = Set.split (tooOldCount,"") set
           in ((newCnt,newSet),())

newOAuthNonce :: MonadIO m => OAuthState -> m Text
newOAuthNonce OAuthStateless = pure ""
newOAuthNonce (OAuthState ref) =
  do rnd <- randomString StringOpts { alphabet=Base58, nrBytes = 24 }
     liftIO $ atomicModifyIORef ref $ \(counter,st) ->
        let ns  = T.pack (show counter) <> "_" <> rnd
            !newSet = Set.insert (counter,rnd) st
        in ((counter,newSet),ns)

type Nonce = Text

-- Return true if the auth nonce is recent, valid, and removes it from the state
verifyOAuthNonce :: MonadIO m => OAuthState -> Maybe Nonce -> m Bool
verifyOAuthNonce OAuthStateless Nothing = pure True
verifyOAuthNonce OAuthStateless (Just _) = pure True -- we ignore state as not a nonce but it could be used by another part of the oauth system
verifyOAuthNonce (OAuthState _) Nothing = pure False
verifyOAuthNonce (OAuthState ref) (Just nonce) =
  do
    let nonceStructure :: (Integer,Text)
        nonceStructure = (\(a,b) -> (maybe (-1) identity (readMaybe (T.unpack a)),T.drop 1 b)) (T.break (== '_') nonce)
    liftIO $ atomicModifyIORef ref $ \(counter,st) ->
      let !newSet = Set.delete nonceStructure st
        in ((counter,newSet), Set.member nonceStructure st)

-- Step 1. Take user to the service's auth page. Returns the URL for oauth to the
-- given provider.

-- | Acquire the URL, which includes the oauth state (a nonce), for the user
-- to log into the identified oauth provider and be redirected back to the
-- requesting server.
type AddApplicationState m e = Text -> m (Either e Text) -- Given the nonce, wrap it in any other application state

getAuthorize :: MonadIO m => OAuthState -> OAuth2 -> AddApplicationState m e -> m (Either e Text)
getAuthorize authSt oinfo aas = do
  nonce <- (newOAuthNonce authSt)
  newState <- aas nonce
  pure $ (authEndpoint oinfo) <$> newState

-- Step 2. Accept a temporary code from the service

-- | Upon redirect there should be at least two parameters
--  * "code=<somecode>" which is handed to the provider to acquire a token.
--  * "state=<thestate>" which is checked to ensure it originated from the
--    getAuthorize call.
--
-- @getAuthorized provider authState codeParam stateParam@ verifies the state
-- is valid and not timed out, requests a token from the service provider
-- using the code, and returns the token obtained from the provider (or
-- @Nothing@ on failure).
getAuthorized :: MonadIO m => TokenByteStringAcquire -> OAuth2 -> OAuthState -> Code -> Maybe Nonce -> m (Either Text Token)
getAuthorized ta prov authSt c nonce =
  do b <- verifyOAuthNonce authSt nonce
     if (not b) then pure (Left "Nonce verification failed")
                else getAccessToken ta c prov

-- Step 3. Exchange code for auth token
    
-- | After the client has logged into the provider, been redirected to the
-- authorized address and provided the state and code parameters the server
-- may acquire a token from the oauth provider.
getAccessToken :: MonadIO m => TokenByteStringAcquire -> Code -> OAuth2 -> m (Either Text Token)
getAccessToken tokenAcq c prov = tokenRequest tokenAcq AuthorizationCode c prov

refreshAuthorized :: MonadIO m => TokenByteStringAcquire -> Text -> OAuth2 -> m (Either Text Token)
refreshAuthorized tokenAcq c prov = tokenRequest tokenAcq RefreshToken c prov 

tokenRequest :: MonadIO m => TokenByteStringAcquire -> GrantType -> Code -> OAuth2 -> m (Either Text Token)
tokenRequest tokenAcq gt c prov = makeTokenRequestCall (tokenEndpoint gt c prov) (oauthAccessTokenEndpoint prov)
  where 
    makeTokenRequestCall :: MonadIO m => TokenRequest -> Text -> m (Either Text Token)
    makeTokenRequestCall tr endpoint = do
      at <- liftIO $ tokenAcq endpoint tr
      return (at >>= parseToken)
    parseToken tk = (maybe (Left ("Token decode failed: "<> (decodeUtf8 $ LB.toStrict tk))) Right (decode tk))
